# Fetching the minified node image on apline linux
FROM node:slim

# Setting up the work directory
WORKDIR /usr/src/app

# Copying all the files in our project
COPY . .

# Installing dependencies
RUN npm install --save express ejs nodemon

# Starting our application
CMD [ "npx", "nodemon" ]

# Exposing server port
EXPOSE 3000