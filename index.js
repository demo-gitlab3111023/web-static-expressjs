const express       = require('express');
const path          = require('path');
const app           = express();

const appRouting    = require('./routers/app-route');

app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'ejs');

app.use('/', appRouting);

app.listen(3000, ()=>{
    console.log('Application running in port : 3000');
});

module.exports=app;